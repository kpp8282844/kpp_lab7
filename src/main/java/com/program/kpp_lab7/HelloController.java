package com.program.kpp_lab7;
import back.*;
import back.ThreadInfo;
import back.ThreadManager;
import javafx.application.Platform;

import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;

public class HelloController {
    @FXML
    private TextArea console;
    @FXML
    private TextField inputThreadsNum;
    @FXML
    private TextField inputBankMoney;
    @FXML
    private TableView<ThreadInfo> threadsTable;
    @FXML
    private TableColumn nameColumn;
    @FXML
    private TableColumn priorityColumn;
    @FXML
    private TableColumn statusColumn;
    @FXML
    private TableColumn changeTimeColumn;
    ThreadManager tManager;

    @FXML
    protected void StartThreads() {
        Bank bank = new Bank(Integer.valueOf(inputBankMoney.getText()));
        bank.setConsole(console);

        if (tManager != null) {
            for (var thread: tManager.getThreads()) {
                if(!thread.isInterrupted()) thread.interrupt();
            }
            console.clear();
        }

        tManager = new ThreadManager(Integer.valueOf(inputThreadsNum.getText()), bank, this.threadsTable);

    }
    @FXML
    protected void KillSelectedThread() throws InterruptedException {
        tManager.KillSelectedThread();
    }
    @FXML
    public void ResumeSelectedThread() throws InterruptedException {
        tManager.ResumeSelectedThread();
    }
    @FXML
    public void SuspendSelectedThread() throws InterruptedException {
        tManager.SuspendSelectedThread();
    }
    @FXML
    public void initialize(){
        nameColumn.setCellValueFactory(new PropertyValueFactory<ThreadInfo, String>("name"));
        priorityColumn.setCellValueFactory(new PropertyValueFactory<ThreadInfo, String>("priority"));
        statusColumn.setCellValueFactory(new PropertyValueFactory<ThreadInfo, String>("status"));
        changeTimeColumn.setCellValueFactory(new PropertyValueFactory<ThreadInfo, String>("lastStatusChangeTime"));

    }
}