module com.program.kpp_lab7 {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.xml;


    opens com.program.kpp_lab7 to javafx.fxml;
    exports com.program.kpp_lab7;

    opens back to javafx.base;
    exports back;

}