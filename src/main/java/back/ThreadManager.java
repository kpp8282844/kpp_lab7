package back;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableView;

import java.util.Date;
import java.util.Objects;
import java.util.Random;


import java.util.LinkedList;

public class ThreadManager {
    LinkedList<Thread> threads;
    LinkedList<Client> clients;

    @FXML
    TableView<ThreadInfo> threadsTable;
    ObservableList<ThreadInfo> threadInfoList;

    public LinkedList<Client> getClients() {
        return clients;
    }

    public void setClients(LinkedList<Client> clients) {
        this.clients = clients;
    }

    public LinkedList<Thread> getThreads() {
        return threads;
    }

    public void setThreads(LinkedList<Thread> threads) {
        this.threads = threads;
    }

    public ThreadManager(Integer numOfThreads, Bank bank, TableView<ThreadInfo> threadsTable){
        this.threadsTable = threadsTable;
        this.threads = new LinkedList<>();
        this.clients = new LinkedList<>();

        this.threadInfoList = FXCollections.observableArrayList();
        Random random = new Random();

        for(int i = 0; i < numOfThreads; i++){
            Client newClient = new Client(bank, i + 1, this, this.threadsTable);
            clients.add(newClient);
            Thread thread = new Thread(newClient) ;
            int randomPriority = random.nextInt(Thread.MAX_PRIORITY - Thread.MIN_PRIORITY + 1) + Thread.MIN_PRIORITY;
            thread.setPriority(randomPriority);
            thread.setName(String.valueOf(i + 1));
            threads.add(thread);
            ThreadInfo threadInfo = new ThreadInfo(
                    thread.getName(),
                    "Running",
                    thread.getPriority(),
                    (new Date()).toString()
            );
            threadInfoList.add(threadInfo);
        }
        threadsTable.setItems(threadInfoList);
        for (var thread: threads) {
            thread.start();
        }
    }
    public void ResumeSelectedThread(){
        ObservableList<ThreadInfo> selectedThreadsInfo = threadsTable.getSelectionModel().getSelectedItems();
        for (ThreadInfo selectedThreadInfo : selectedThreadsInfo) {
            Client selectedClient = this.getClients().stream()
                    .filter(client -> String.valueOf(client.getClientId()).equals(selectedThreadInfo.getName()))
                    .findFirst()
                    .orElse(null);
            if (selectedClient != null) {
                synchronized (selectedClient.getLock()) {
                    selectedClient.getLock().notify();
                }
                selectedThreadInfo.setStatus("Running");
                threadsTable.refresh();
            }
        }
    }
    public void SuspendSelectedThread() throws InterruptedException {
        ObservableList<ThreadInfo> selectedThreadsInfo = threadsTable.getSelectionModel().getSelectedItems();
        for (ThreadInfo selectedThreadInfo : selectedThreadsInfo) {
            Client selectedClient = this.getClients().stream()
                    .filter(client -> String.valueOf(client.getClientId()).equals(selectedThreadInfo.getName()))
                    .findFirst()
                    .orElse(null);

            if (selectedClient != null) {
                selectedThreadInfo.setStatus("Paused");
                threadsTable.refresh();
            }
        }
    }
    public void KillSelectedThread() {
        ObservableList<ThreadInfo> selectedThreadsInfo = threadsTable.getSelectionModel().getSelectedItems();

        for (ThreadInfo selectedThreadInfo : selectedThreadsInfo) {
            Thread selectedThread = this.getThreads().stream()
                    .filter(thread -> Objects.equals(thread.getName(), selectedThreadInfo.getName()))
                    .findFirst()
                    .orElse(null);

            if (selectedThread != null) {
                selectedThread.interrupt();
                selectedThreadInfo.setStatus("Wasted");
                threadsTable.refresh();
            }
        }
        threadsTable.refresh();
    }

}