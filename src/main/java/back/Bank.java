package back;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.TextArea;

import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicInteger;

public class Bank {

    private Semaphore sem;

    @FXML
    TextArea console;
    private volatile AtomicInteger money  = new AtomicInteger();

    public Bank(){
        this.money.set(10000);
        sem = new Semaphore(1, true);
    }
    public Bank(int money){
        this.money.set(money);
        sem = new Semaphore(1, true);
    }

    public int getMoney() {
        return money.intValue();
    }

    public void setMoney(int money) {
        this.money.set(money);
    }

    public Semaphore getSem() {
        return sem;
    }

    public void setConsole(TextArea console) {
        this.console = console;
    }

    public  void giveMoney(Client client, int cash) throws InterruptedException {
       try {
           sem.acquire();


           if (this.money.intValue() < cash){
               String str = "Client " + client.getClientId() + " cant receive " + cash + "$. Bank balance: " + this.money;
               this.money.addAndGet(1000);
               writeToConsole(str);
           }
           else {
               client.addCash(cash);
               money.addAndGet((cash * -1));
               String str = "Client " + client.getClientId() + " received " + cash + "$. Bank balance: " + this.money;
               writeToConsole(str);
           }

       } finally {
           sem.release();
       }
    }

    public  void takeMoney(Client client, int cash) throws InterruptedException {
        try {
            sem.acquire();
            this.money.addAndGet(cash);
            client.minusCash(cash);
            String str = "Client " + client.getClientId() + " gave " + cash + "$. Bank balance: " + this.money;
            writeToConsole(str);
        } finally {
            sem.release();
        }

    }

    private synchronized void writeToConsole(String text) throws InterruptedException {
        if (console.getText().length() > 8000) {
            String newText = console.getText().substring(console.getText().length() - 4000);
            console.setText(newText);
        }
        Platform.runLater(() -> {
            console.appendText(text + "\n");
        });
    }
}
