package back;

import javafx.application.Platform;
import javafx.scene.control.TableView;

import java.util.Date;
import java.util.Objects;
import java.util.Random;

public class Client implements Runnable {

    private final Object lock = new Object();
    private final Bank bank;
    private final int clientId;
    private int cash;
    private final Random rand;
    ThreadManager tManager;
    TableView<ThreadInfo> table;

    public Client(Bank bank, int clientId, ThreadManager tMan, TableView<ThreadInfo> table){
        this.bank = bank;
        this.clientId = clientId;
        this.tManager = tMan;
        this.table = table;
        rand = new Random();
        cash = rand.nextInt(10001);
    }

    public Object getLock() {
        return lock;
    }

    public int getClientId() {
        return clientId;
    }

    public int getCash() {
        return cash;
    }

    public void setCash(int cash) {
        this.cash = cash;
    }

    public void addCash(int cash){
        this.cash += cash;
    }

    public void minusCash(int cash){
        this.cash -= cash;
    }

    @Override
    public void run() {
        try {
            while (true){
                this.StopMe();
                if (this.getCash() < 1001){
                    bank.giveMoney(this, rand.nextInt(1000,10001));
                }
                else  {
                    bank.takeMoney(this, rand.nextInt( cash));
                }

                Thread.sleep(rand.nextInt(1000, 4000));
            }
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
        updateLastStatusChangeTime(Thread.currentThread().getName(), new Date());
    }
    private void updateLastStatusChangeTime(String threadName, Date time) {
        for (ThreadInfo threadInfo : tManager.threadInfoList) {
            if (threadInfo.getName().equals(threadName)) {
                threadInfo.setLastStatusChangeTime(time.toString());
                break;
            }
        }
        Platform.runLater(() -> tManager.threadsTable.refresh());
    }


    public void StopMe() {
        TableView.TableViewSelectionModel<ThreadInfo> selectionModel = table.getSelectionModel();
        if (!selectionModel.isEmpty()) {
            synchronized (lock) {
                if (Objects.equals(table.getItems().get(this.getClientId() - 1).getStatus(), "Paused")) {
                    try {
                        lock.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }



}
